# Contributing

Thanks for taking the time to contribute!

## Style

### Code

`cargo fmt`, please. CI will catch this if you forget :)

### Commit messages

Be clear and concise in the commit summary, but feel free to go into detail in the description.

### Issues and merge requests

Provide plenty of context, such as error messages, so that the issue/MR may be effectively evaluated.

## Updating versions

The following versions must be changed in multiple places when updating them.

### Crate version

- It must be changed in the following locations:
  - The `version` in the crate manifest: ([`Cargo.toml`](Cargo.toml))
  - The "Dependency Status" badge in the crate-level documentation (in **both** URLs): ([`src/lib.rs`](src/lib.rs))
- Make sure to add links in [`CHANGELOG.md`](CHANGELOG.md) to account for the new version.

### Minimum supported Rust version

- It must be changed in the following locations:
  - The `rust-version` in the crate manifest: ([`Cargo.toml`](Cargo.toml))
  - The MSRV badge in the README (both in the alt text **and** the URL): ([`README.md`](README.md))
  - The MSRV badge in the crate-level documentation (both in the alt text **and** the URL): ([`src/lib.rs`](src/lib.rs))
  - The MSRV pipeline in the CI config: ([`.gitlab-ci.yml`](.gitlab-ci.yml))
- Make sure to add the change to [`CHANGELOG.md`](CHANGELOG.md).
  ```md
  ### Changed

  - The minimum supported Rust version is 1.xx.
  ```
