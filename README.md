# `maybe_uninit_ext`

[![crates.io](https://img.shields.io/crates/v/maybe-uninit-ext.svg)](https://crates.io/crates/maybe-uninit-ext)
[![docs.rs](https://docs.rs/maybe-uninit-ext/badge.svg)](https://docs.rs/maybe-uninit-ext)
[![Dependency Status](https://deps.rs/repo/gitlab/wackbyte/maybe-uninit-ext/status.svg)](https://deps.rs/repo/gitlab/wackbyte/maybe-uninit-ext)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/wackbyte/maybe-uninit-ext?branch=trunk)](https://gitlab.com/wackbyte/maybe-uninit-ext/-/pipelines)
[![Unlicense](https://img.shields.io/badge/license-Unlicense-blue.svg)](https://unlicense.org/)
[![Minimum Supported Rust Version is 1.75](https://img.shields.io/badge/MSRV-1.75-white.svg)](https://doc.rust-lang.org/cargo/reference/manifest.html#the-rust-version-field)

Extended maybe-uninit types.
