# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog],
and this project adheres to [Semantic Versioning].

## [Unreleased]

### Removed

- The `nightly` feature flag.

## [0.6.0]

Released on 2023-12-29.

### Changed

- `zeroed` is `const`.
- The minimum supported Rust version is 1.75.

## [0.5.0]

Released on 2023-07-29.

### Changed

- `assume_init_read` is `const`.
- The minimum supported Rust version is 1.71.

## [0.4.0]

Released on 2023-05-07.

### Removed

- `AssumeInitExt`, `SafeAssumeInitExt`, and `UninitExt`.
- `prelude`.

## [0.3.0]

Released on 2023-04-18.

### Added

- The `AssumeInit`, `SafeAssumeInit`, and `Uninit` traits, each with an associated module and `Ext` trait.
- `prelude` contains commonly exported types.
- The `nightly` feature flag makes many functions `const` but requires the nightly toolchain.

### Changed

- Split `MaybeUninitExt` into `AssumeInit` and `Uninit`.
- Change the trait bound of each function from `MaybeUninitExt` to either `AssumeInit` or `Uninit` depending on which it requires.
- Move each function to the module that corresponds with its new trait bound.
- `write` no longer drops the old value.

## [0.2.0]

Released on 2023-04-08.

### Added

- The following functions:
  - `zeroed`
  - `new`
  - `as_ptr`
  - `as_mut_ptr`
  - `assume_init_read`
  - `assume_init_drop`
  - `write`
- The minimum supported Rust version is 1.61.

### Changed

- `assume_init_ref` is `const`.
- `uninit` has `#[must_use]`.

## [0.1.0]

Released on 2023-04-07.

### Added

- The following functions:
  - `uninit`
  - `assume_init`
  - `assume_init_ref`
  - `assume_init_mut`
- `MaybeUninitExt` is implemented for `MaybeUninit`-like types.

[Unreleased]: https://gitlab.com/wackbyte/maybe-uninit-ext/-/compare/v0.6.0...trunk
[0.6.0]: https://gitlab.com/wackbyte/maybe-uninit-ext/-/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/wackbyte/maybe-uninit-ext/-/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/wackbyte/maybe-uninit-ext/-/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/wackbyte/maybe-uninit-ext/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/wackbyte/maybe-uninit-ext/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/wackbyte/maybe-uninit-ext/-/tags/v0.1.0

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
