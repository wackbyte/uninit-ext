//! Implementations of [`AssumeInit`].
//!
//! # Missing implementations
//!
//! - Zero-sized arrays?
//!   - See what [`uninit`](mod@crate::uninit) has to say.
//! - Tuples?
//!   - Unsound. The layout of `(MaybeUninit<T>, MaybeUninit<U>)` is not
//!     guaranteed to be the same as that of `(T, U)`.
//! - References?
//!   - Unsound. See this example:
//!     ```compile_fail
//!     # use core::mem::MaybeUninit;
//!     let mut a = 0;
//!     let b = maybe_uninit_ext::new::<&mut MaybeUninit<u8>>(&mut a);
//!     *b = maybe_uninit_ext::uninit();
//!     assert_eq!(a, 0); // Undefined behavior! `a` is uninitialized.
//!     ```
//!     Due to interior mutability, this is an issue for immutable references
//!     as well.

use super::AssumeInit;

// SAFETY:
// - `MaybeUninit<T>` is `repr(transparent)` over `T`.
unsafe impl<T> AssumeInit for core::mem::MaybeUninit<T> {
	type Init = T;
}

//
// Primitive Implementations
//

// SAFETY:
// - `()` is a zero-sized type.
unsafe impl AssumeInit for () {
	type Init = Self;
}

// SAFETY:
// - As `T` and `T::Init` have the same representation, so will `[T; N]` and
//   `[T::Init; N]`.
unsafe impl<T, const N: usize> AssumeInit for [T; N]
where
	T: AssumeInit,
{
	type Init = [T::Init; N];
}

/*
// SAFETY:
// - `[T; 0]` is a zero-sized type.
unsafe impl<T> AssumeInit for [T; 0] {
	type Init = Self;
}
*/

//
// Core Library Implementations
//

// SAFETY:
// - `PhantomData<T>` is a zero-sized type.
unsafe impl<T> AssumeInit for core::marker::PhantomData<T>
where
	T: ?Sized,
{
	type Init = Self;
}

// SAFETY:
// - `PhantomPinned` is a zero-sized type.
unsafe impl AssumeInit for core::marker::PhantomPinned {
	type Init = Self;
}

// SAFETY:
// - `Cell<T>` is `repr(transparent)` over `T`.
unsafe impl<T> AssumeInit for core::cell::Cell<T>
where
	T: AssumeInit,
{
	type Init = core::cell::Cell<T::Init>;
}

// SAFETY:
// - `UnsafeCell<T>` is `repr(transparent)` over `T`.
unsafe impl<T> AssumeInit for core::cell::UnsafeCell<T>
where
	T: AssumeInit,
{
	type Init = core::cell::UnsafeCell<T::Init>;
}

// SAFETY:
// - `ManuallyDrop<T>` is `repr(transparent)` over `T`.
unsafe impl<T> AssumeInit for core::mem::ManuallyDrop<T>
where
	T: AssumeInit,
{
	type Init = core::mem::ManuallyDrop<T::Init>;
}
