//! [`AssumeInit`]: unsafely extract an initialized value from a value that may
//! be uninitialized.

use core::{mem, ptr};

mod impls;

/// Types that may be in an uninitialized state.
///
/// # Safety
///
/// The type and its [`Init`](Self::Init) type must have the same
/// representation.
///
/// Generally speaking, it must be safe to [transmute](mem::transmute) between
/// them and between references to them.
///
/// If this invariant is not upheld, [undefined behavior] is likely to occur.
///
/// [undefined behavior]: https://doc.rust-lang.org/stable/reference/behavior-considered-undefined.html
pub unsafe trait AssumeInit: Sized {
	/// The initialized state of the type.
	type Init;
}

/// Creates an initialized instance of `T`.
#[inline(always)]
#[must_use]
pub fn new<T>(value: T::Init) -> T
where
	T: AssumeInit,
{
	// SAFETY:
	// - The implementer ensures that the types have the same representation.
	let maybe_uninit = unsafe { mem::transmute_copy::<T::Init, T>(&value) };

	// Don't run the destructor of the original value.
	mem::forget(value);

	maybe_uninit
}

/// Initializes the value. Returns a mutable reference to the value after it has
/// been initialized.
///
/// The original value will *not* be dropped. However, most types implementing
/// [`AssumeInit`] (such as [`MaybeUninit`](core::mem::MaybeUninit) itself) do
/// not drop the value they contain.
#[inline(always)]
pub fn write<T>(maybe_uninit: &mut T, value: T::Init) -> &mut T::Init
where
	T: AssumeInit,
{
	// SAFETY:
	// - The pointer is valid as it is obtained from a reference.
	unsafe {
		ptr::write(maybe_uninit, new(value));
	}

	// SAFETY:
	// - The value was just initialized.
	unsafe { assume_init_mut(maybe_uninit) }
}

/// Returns an immutable pointer to the initialized value.
///
/// Dereferencing or reading from this pointer is undefined behavior unless the
/// value is fully initialized.
#[inline(always)]
pub const fn as_ptr<T>(maybe_uninit: &T) -> *const T::Init
where
	T: AssumeInit,
{
	(maybe_uninit as *const T).cast::<T::Init>()
}

/// Returns a mutable pointer to the initialized value.
///
/// Dereferencing or reading from this pointer is undefined behavior unless the
/// value is fully initialized.
#[inline(always)]
pub fn as_mut_ptr<T>(maybe_uninit: &mut T) -> *mut T::Init
where
	T: AssumeInit,
{
	(maybe_uninit as *mut T).cast::<T::Init>()
}

/// Assumes the value is initialized.
///
/// # Safety
///
/// The value must be fully initialized.
#[inline(always)]
pub unsafe fn assume_init<T>(maybe_uninit: T) -> T::Init
where
	T: AssumeInit,
{
	// SAFETY:
	// - The caller ensures that the value is fully initialized.
	// - The implementer ensures that the types have the same representation.
	let init = unsafe { mem::transmute_copy::<T, T::Init>(&maybe_uninit) };

	// Don't run the destructor of the original value.
	mem::forget(maybe_uninit);

	init
}

/// Assumes the value is initialized. Returns a reference.
///
/// # Safety
///
/// The value must be fully initialized.
#[inline(always)]
pub const unsafe fn assume_init_ref<T>(maybe_uninit: &T) -> &T::Init
where
	T: AssumeInit,
{
	// SAFETY:
	// - The caller ensures that the value is fully initialized.
	// - The implementer ensures that the types have the same representation.
	unsafe { &*as_ptr(maybe_uninit) }
}

/// Assumes the value is initialized. Returns a mutable reference.
///
/// # Safety
///
/// The value must be fully initialized.
#[inline(always)]
pub unsafe fn assume_init_mut<T>(maybe_uninit: &mut T) -> &mut T::Init
where
	T: AssumeInit,
{
	// SAFETY:
	// - The caller ensures that the value is fully initialized.
	// - The implementer ensures that the types have the same representation.
	unsafe { &mut *as_mut_ptr(maybe_uninit) }
}

/// Reads the value and assumes that it is initialized.
///
/// If you have ownership of the value, it is preferable that you use
/// [`assume_init`] instead, preventing duplication of the value.
///
/// # Safety
///
/// The value must be fully initialized.
///
/// If the value is read multiple times, duplicating it, it is up
/// to you to ensure that the value is not dropped multiple times, possibly
/// causing a double-free. If the type implements `Copy`, this should not be a
/// problem.
#[inline(always)]
pub const unsafe fn assume_init_read<T>(maybe_uninit: &T) -> T::Init
where
	T: AssumeInit,
{
	// SAFETY:
	// - The pointer is valid as it was created from a reference.
	// - The caller ensures that the value is fully initialized.
	// - The implementer ensures that the types have the same representation.
	unsafe { ptr::read(as_ptr(maybe_uninit)) }
}

/// Assumes the value is initialized and drops it in place.
///
/// If you have ownership of the value, it is preferable that you use
/// [`assume_init`] instead.
///
/// # Safety
///
/// The value must be fully initialized.
///
/// The value must be valid for dropping. This is type-specific.
#[inline(always)]
pub unsafe fn assume_init_drop<T>(maybe_uninit: &mut T)
where
	T: AssumeInit,
{
	// SAFETY:
	// - The pointer is valid as it was created from a reference.
	// - The caller ensures that the value is fully initialized.
	// - The caller ensures that the value is valid for dropping.
	// - The implementer ensures that the types have the same representation.
	unsafe { ptr::drop_in_place(as_mut_ptr(maybe_uninit)) }
}
