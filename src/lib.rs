//! # `maybe_uninit_ext`
//!
//! [![crates.io](https://img.shields.io/crates/v/maybe-uninit-ext.svg)](https://crates.io/crates/maybe-uninit-ext)
//! [![Dependency Status](https://deps.rs/crate/maybe-uninit-ext/0.6.0/status.svg)](https://deps.rs/crate/maybe-uninit-ext/0.6.0)
//! [![Unlicense](https://img.shields.io/badge/license-Unlicense-blue.svg)](https://unlicense.org/)
//! [![Minimum Supported Rust Version is 1.75](https://img.shields.io/badge/MSRV-1.75-white.svg)](https://doc.rust-lang.org/cargo/reference/manifest.html#the-rust-version-field)
//!
//! Support for "extended" maybe-uninit types.
//!
//! # Examples
//!
//! ```
//! # use core::mem::MaybeUninit;
//! // Creates a 4x4 grid of `MaybeUninit` values.
//! let mut grid = maybe_uninit_ext::uninit::<[[MaybeUninit<usize>; 4]; 4]>();
//! grid.iter_mut().enumerate().for_each(|(i, row)| {
//!     row.iter_mut().enumerate().for_each(|(j, tile)| {
//!         tile.write(i * j);
//!     });
//! });
//!
//! // SAFETY: The grid was fully initialized.
//! let grid = unsafe { maybe_uninit_ext::assume_init(grid) };
//! assert_eq!(
//!     grid,
//!     [[0, 0, 0, 0], [0, 1, 2, 3], [0, 2, 4, 6], [0, 3, 6, 9]],
//! );
//! ```

// Attributes
#![cfg_attr(not(any(doc, test)), no_std)]
// Lints
#![allow(clippy::inline_always, clippy::module_name_repetitions)]
#![warn(clippy::missing_docs_in_private_items, missing_docs)]
#![deny(
	clippy::multiple_unsafe_ops_per_block,
	clippy::undocumented_unsafe_blocks,
	unsafe_op_in_unsafe_fn
)]

#[cfg(feature = "nightly")]
#[macro_use]
extern crate qualifier_attr;

pub mod assume_init;
pub mod safe_assume_init;
pub mod uninit;

#[doc(inline)]
pub use self::{assume_init::*, safe_assume_init::*, uninit::*};

#[cfg(test)]
mod tests {
	#![allow(
		clippy::multiple_unsafe_ops_per_block,
		clippy::undocumented_unsafe_blocks
	)]

	use {
		super::*,
		core::{cell::UnsafeCell, fmt::Debug, mem::MaybeUninit},
	};

	/// Assert that the contained value is equal to the given value.
	///
	/// # Safety
	///
	/// The value must be fully initialized.
	#[track_caller]
	unsafe fn assert_init_eq<T>(mut mu: T, mut val: T::Init)
	where
		T: AssumeInit,
		T::Init: PartialEq + Debug,
	{
		unsafe {
			assert_eq!(assume_init_ref(&mu), &val);
			assert_eq!(assume_init_mut(&mut mu), &mut val);
			assert_eq!(assume_init(mu), val);
		}
	}

	#[test]
	fn unit() {
		#[allow(clippy::let_unit_value)]
		let x = uninit::<()>();

		unsafe {
			assert_init_eq(x, ());
		}
	}

	#[test]
	fn maybe_uninit() {
		let mut x = uninit::<MaybeUninit<usize>>();
		x.write(23);

		unsafe {
			assert_init_eq(x, 23);
		}
	}

	#[test]
	fn maybe_uninit_array() {
		let mut xs = uninit::<[MaybeUninit<usize>; 4]>();
		xs.iter_mut().enumerate().for_each(|(i, x)| {
			x.write(i);
		});

		unsafe {
			assert_init_eq(xs, [0, 1, 2, 3]);
		}
	}

	#[test]
	fn maybe_uninit_array_array() {
		let mut xss = uninit::<[[MaybeUninit<usize>; 4]; 4]>();
		xss.iter_mut().enumerate().for_each(|(i, xs)| {
			let len = xs.len();
			xs.iter_mut().enumerate().for_each(|(j, x)| {
				x.write(i * len + j);
			});
		});

		unsafe {
			assert_init_eq(
				xss,
				[[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11], [12, 13, 14, 15]],
			);
		}
	}

	#[test]
	fn unsafe_cell_shenanigans() {
		let xxs = new::<UnsafeCell<[MaybeUninit<i32>; 6]>>(UnsafeCell::new([7, 7, 7, 7, 7, 7]));
		write(unsafe { &mut *xxs.get() }, [6, 6, 6, 6, 6, 6]);

		unsafe {
			assert_init_eq(xxs.into_inner(), [6, 6, 6, 6, 6, 6]);
		}
	}

	#[test]
	fn zeroed_out() {
		let xss = zeroed::<[[MaybeUninit<u8>; 2]; 2]>();

		unsafe {
			assert_init_eq(xss, [[0, 0], [0, 0]]);
		}
	}

	#[test]
	fn maybe_uninit_tuple() {
		let (a, b, c, d) = zeroed::<(
			MaybeUninit<u64>,
			MaybeUninit<u32>,
			MaybeUninit<u16>,
			MaybeUninit<u8>,
		)>();

		unsafe {
			assert_init_eq(a, 0);
			assert_init_eq(b, 0);
			assert_init_eq(c, 0);
			assert_init_eq(d, 0);
		}
	}

	#[test]
	fn safely_assume_init() {
		let mut a = uninit::<MaybeUninit<MaybeUninit<u8>>>();
		safe_assume_init_mut(&mut a).write(12);

		let a = safe_assume_init(a);
		unsafe {
			assert_init_eq(a, 12);
		}
	}
}
