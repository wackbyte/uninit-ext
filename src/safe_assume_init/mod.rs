//! [`SafeAssumeInit`]: safely assume that uninitialized values
//! are initialized, regardless of whether they actually are.

use crate::{
	assume_init::{assume_init, assume_init_mut, assume_init_ref, AssumeInit},
	uninit::Uninit,
};

/// Types whose initialized state may be uninitialized, ensuring that they may
/// *always* be assumed to be initialized.
///
/// Automatically implemented for all [`AssumeInit`] types whose
/// [`Init`](AssumeInit::Init) type is [`Uninit`].
pub trait SafeAssumeInit: AssumeInit
where
	Self::Init: Uninit,
{
}

// SAFETY:
// - The type's initialized state may be created in an uninitialized state, so
//   the value's initialization state does not matter when assuming that it is
//   initialized.
impl<T> SafeAssumeInit for T
where
	T: AssumeInit,
	T::Init: Uninit,
{
}

/// Assumes that the value is initialized, regardless of whether it actually is.
///
/// # Safety
///
/// [`SafeAssumeInit`] guarantees that this is safe to perform.
#[inline(always)]
pub fn safe_assume_init<T>(maybe_uninit: T) -> T::Init
where
	T: SafeAssumeInit,
	T::Init: Uninit,
{
	// SAFETY:
	// - The implementer ensures that this operation is safe.
	unsafe { assume_init(maybe_uninit) }
}

/// Assumes that the value is initialized, regardless of whether it actually is.
/// Returns a reference.
///
/// # Safety
///
/// [`SafeAssumeInit`] guarantees that this is safe to perform.
#[inline(always)]
pub const fn safe_assume_init_ref<T>(maybe_uninit: &T) -> &T::Init
where
	T: SafeAssumeInit,
	T::Init: Uninit,
{
	// SAFETY:
	// - The implementer ensures that this operation is safe.
	unsafe { assume_init_ref(maybe_uninit) }
}

/// Assumes that the value is initialized, regardless of whether it actually is.
/// Returns a mutable reference.
///
/// # Safety
///
/// [`SafeAssumeInit`] guarantees that this is safe to perform.
#[inline(always)]
pub fn safe_assume_init_mut<T>(maybe_uninit: &mut T) -> &mut T::Init
where
	T: SafeAssumeInit,
	T::Init: Uninit,
{
	// SAFETY:
	// - The implementer ensures that this operation is safe.
	unsafe { assume_init_mut(maybe_uninit) }
}
