//! [`Uninit`]: create uninitialized values.

use core::mem::MaybeUninit;

mod impls;

/// Types that may be constructed in an uninitialized state.
///
/// # Safety
///
/// It must be safe to construct the type in an uninitialized state.
/// This is true if your type only contains:
///
/// - Zero-sized types.
/// - [`MaybeUninit`] itself.
/// - Other [`Uninit`] types.
///
/// Note that the type must be inhabited. Implementing this trait for
/// uninhabited types such as [`!`] and `enum {}` would cause undefined
/// behavior upon creation.
///
/// Furthermore, care should be taken to not implement this trait for types
/// that have creation invariants. `GhostToken` from [GhostCell], for example,
/// while being a zero-sized type, is unsound to create from nothing.
///
/// If this invariant is not upheld, [undefined behavior] is likely to occur.
///
/// [GhostCell]: https://plv.mpi-sws.org/rustbelt/ghostcell/
/// [undefined behavior]: https://doc.rust-lang.org/stable/reference/behavior-considered-undefined.html
pub unsafe trait Uninit: Sized {}

/// Creates an uninitialized instance of `T`.
#[inline(always)]
#[must_use]
pub const fn uninit<T>() -> T
where
	T: Uninit,
{
	// SAFETY:
	// - The implementer ensures that it is safe to create an uninitialized instance
	//   of `T`.
	unsafe {
		#[allow(clippy::uninit_assumed_init)]
		MaybeUninit::uninit().assume_init()
	}
}

/// Creates an uninitialized instance of `T`, filling it with `0` bytes.
///
/// Depending on `T`, this may be considered proper initialization. For example,
/// `zeroed::<MaybeUninit<usize>>()` would be initialized, but
/// `zeroed::<MaybeUninit<&'static i32>>()` is not as references must not be
/// null.
#[inline(always)]
#[must_use]
pub const fn zeroed<T>() -> T
where
	T: Uninit,
{
	// SAFETY:
	// - The implementer ensures that it is safe to create an uninitialized instance
	//   of `T`.
	unsafe { MaybeUninit::zeroed().assume_init() }
}
