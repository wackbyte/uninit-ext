//! Implementations of [`Uninit`].
//!
//! # Missing implementations
//!
//! - Zero-sized arrays?
//!   - Currently impossible without specialization. Such an implementation
//!     would conflict with the one for `[T; N]`.

use super::Uninit;

// SAFETY:
// - `MaybeUninit<T>` may be safely uninitialized.
unsafe impl<T> Uninit for core::mem::MaybeUninit<T> {}

//
// Primitive Implementations
//

// SAFETY:
// - `()` is a zero-sized type.
unsafe impl Uninit for () {}

// SAFETY:
// - `[T; N]` contains only `T`.
unsafe impl<T, const N: usize> Uninit for [T; N] where T: Uninit {}

/*
// SAFETY:
// - `[T; 0]` is a zero-sized type.
unsafe impl<T> Uninit for [T; 0] {}
*/

/// Implements [`Uninit`] for a series of tuple types.
macro_rules! tuples {
	(
		$(
			$($T:ident)+;
		)*
	) => {
		$(
			// SAFETY:
			// - Each type is `Uninit`
			unsafe impl<$($T),+> Uninit for ($($T,)+)
			where
				$($T: Uninit),+
			{}
		)*
	};
}

// 12
tuples! {
	A;
	A B;
	A B C;
	A B C D;
	A B C D E;
	A B C D E F;
	A B C D E F G;
	A B C D E F G H;
	A B C D E F G H I;
	A B C D E F G H I J;
	A B C D E F G H I J K;
	A B C D E F G H I J K L;
}

//
// Core Library Implementations
//

// SAFETY:
// - `PhantomData<T>` is a zero-sized type.
unsafe impl<T> Uninit for core::marker::PhantomData<T> where T: ?Sized {}

// SAFETY:
// - `PhantomPinned` is a zero-sized type.
unsafe impl Uninit for core::marker::PhantomPinned {}

// SAFETY:
// - `Cell<T>` is `repr(transparent)` over `T`.
unsafe impl<T> Uninit for core::cell::Cell<T> where T: Uninit {}

// SAFETY:
// - `UnsafeCell<T>` is `repr(transparent)` over `T`.
unsafe impl<T> Uninit for core::cell::UnsafeCell<T> where T: Uninit {}

// SAFETY:
// - `ManuallyDrop<T>` is `repr(transparent)` over `T`.
unsafe impl<T> Uninit for core::mem::ManuallyDrop<T> where T: Uninit {}
